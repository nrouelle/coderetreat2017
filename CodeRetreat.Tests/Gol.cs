﻿using System.Collections.Generic;
using NFluent;
using NUnit.Framework;

namespace CodeRetreat.Tests
{
    [TestFixture]
    public class Gol
    {
        [Test]
        public void ShouldDieWhenLessThanTwoNeighbours()
        {
            var livingNeighbours = new List<LivingCell> { new LivingCell() };
            var cell = new Cell();
            var nextCell = cell.GetNextGeneration(livingNeighbours);
            Check.That(nextCell is DeadCell).IsTrue();
        }

        [Test]
        [TestCase(2)]
        [TestCase(3)]
        public void ShouldStayAliveWhenTwoOrThreeNeighbours(int nbNeighbours)
        {
            var livingNeighbours = new List<LivingCell>();
            for (var i = 0; i < nbNeighbours; i++)
            {
                livingNeighbours.Add(new LivingCell());
            }
            var cell = new LivingCell();
            var nextCell = cell.GetNextGeneration(livingNeighbours);
            Check.That(nextCell is LivingCell).IsTrue();
        }

        [Test]
        [TestCase(4)]
        [TestCase(5)]
        [TestCase(6)]
        [TestCase(7)]
        public void ShouldDieWhenMoreThanThreeNeighbours(int nbNeighbours)
        {
            var livingNeighbours = new List<LivingCell>();
            for (var i = 0; i < nbNeighbours; i++)
            {
                livingNeighbours.Add(new LivingCell());
            }
            var cell = new LivingCell();
            var nextCell = cell.GetNextGeneration(livingNeighbours);
            Check.That(nextCell is DeadCell).IsTrue();
        }

        [Test]
        public void ShouldReliveWhenExactlyThreeNeighbours()
        {
            var livingNeighbours = new List<LivingCell> { new LivingCell(), new LivingCell(), new LivingCell() };

            var cell = new DeadCell();
            var nextCell = cell.GetNextGeneration(livingNeighbours);
            Check.That(nextCell is LivingCell).IsTrue();
        }
    }

    public class Cell
    {
        public Cell GetNextGeneration(List<LivingCell> livingNeighbours)
        {
            if (livingNeighbours.Count == Population.Minpopulation.Count || livingNeighbours.Count == Population.Surpopulation.Count)
                return new LivingCell();
            return new DeadCell();
        }
    }

    public class LivingCell : Cell
    {

    }

    public class DeadCell : Cell
    {

    }

    public static class Population
    {
        public static List<LivingCell> Surpopulation => new List<LivingCell> { new LivingCell(), new LivingCell(), new LivingCell() };
        public static List<LivingCell> Minpopulation => new List<LivingCell> { new LivingCell(), new LivingCell() };
    }
}
